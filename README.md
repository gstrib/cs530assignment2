In this project we generate an amount of prime numbers K each with at least B bits.
Because these numbers are very big, we cannot calculate primes using int or longs
but we must use an outside library with the object mpz_t.  This library supports
finding the primes so all we have to worry about is generating them in the fastest 
way possible.  To do this we used 4 threads running the same process to simultaneously
generate numbers at the same time.  To save them in order for use later we attach them
to a linked list.  The main function then dequeues the linked list using FIFO to 
get the numbers in the order they were enqueued by the threads.
prog2_1.c - contains all the methods and data structure for the linked list to be used
            in prog2_2.c.
            compile with "gcc prog2_1.c -c"
prog2_1.h - header file to declare the data structures for the linked list and it's nodes, also defines the functions that prog2_1.c will contain.
prog2_2.c - handles starting the threads to start to generation of primes numbers.  Also
            holds the linked list object and enqueues/dequeues from it from the main function.
            compile with 
            "gcc prog2_2.c -L./3rdParty/gmp-6.1.2/-l/3rdParty/gmp-6.1.2/ -lgmp -lpthread -o ./build/prog2_2 -o prog 2_2"
            run with "./prog2_2 K B"  (K being an integer representing the amount of prime numbers to generate and B an integer of the least amount of bits each should contain)
