#include <stdio.h>
#include <stdlib.h>
#include<pthread.h>
#include "prog2_1.h"

TSAFELIST* tSafeConstruct()
{
    TSAFELIST *newList = (TSAFELIST*) malloc(sizeof(TSAFELIST));
    pthread_mutex_init(&(newList->mutex), NULL);
    newList->head = NULL;
    return newList;
} 
void tSafeDestruct(TSAFELIST* list)
{
pthread_mutex_lock(&(list->mutex));
while(list->head!=NULL)
{
TSAFENODE *next = list->head->next;
free(list->head);
list->head = next;
}
free(list->last);
free(list);
pthread_mutex_unlock(&(list->mutex));
}
void tSafeEnqueue(TSAFELIST * list,mpz_t val)
{
    pthread_mutex_lock(&(list->mutex));
    //printf("enqueued\n");

    TSAFENODE *newNode = (TSAFENODE*) malloc(sizeof(TSAFENODE));
    mpz_init(newNode->number);
    mpz_add(newNode->number, newNode->number, val);

    if(list->head==NULL)
    {
        //printf("null head\n");
        list->head = newNode;
        list->last = newNode;
    }
    else
    {
        //printf("not null head\n");
        list->last->next = newNode;
        list->last = newNode;
    }    

    pthread_mutex_unlock(&(list->mutex));
}

TSAFEDATA* tSafeDequeue(TSAFELIST* list)
{
pthread_mutex_lock(&(list->mutex));
TSAFEDATA* datapoint;
TSAFEDATA data = {.isValid = 0, .value = NULL};
datapoint = &data;
data.isValid = 10;

if(list->head == NULL)
{
data.isValid=0;
}
else
{
mpz_set(data.value, list->head->number);
data.isValid = 1;
if(list->head == list->last)
{
    //printf("last one");
    list->head = NULL;
}
else
{
    TSAFENODE* cur = list->head->next;
    free(list->head);
    list->head = cur;
}
//printf("deallocating2\n");
}

pthread_mutex_unlock(&(list->mutex));
return datapoint;
}
