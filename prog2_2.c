#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <gmp.h>
#include "prog2_1.c"
mpz_t globalCounter;
int count;
int dummy;
pthread_mutex_t counterguard;// = PTHREAD_MUTEX_INITIALIZER;
TSAFELIST *numberList;

void * generatePrimes(void * name)
{

while(1)
{
    pthread_mutex_lock(&counterguard);
    mpz_nextprime(globalCounter, globalCounter);
    pthread_mutex_unlock(&counterguard);
    //gmp_printf("%Z i\n", globalCounter);

    count++;
    tSafeEnqueue(numberList, globalCounter);
    
}
    
}

int main(int argc, char** argv)
{
numberList = tSafeConstruct();
pthread_mutex_init(&counterguard, NULL);
count = 0;
dummy = 0;
int amtPrimes = atoi(argv[1]);
int k = atoi(argv[2]);
mpz_init_set_str(globalCounter, "1", 10);
mpz_mul_2exp(globalCounter, globalCounter, k-1);
//gmp_printf("%Z i\n", globalCounter);
pthread_t thread_id;

for(int i = 0; i < 4; i++)
{
    pthread_create(&thread_id, NULL, generatePrimes, NULL);
}

TSAFEDATA *cur;

printf("Assignment #2-2, Garrett Stribling, garrett.strib@gmail.com\n");

while(dummy < amtPrimes)
{
    
    usleep(50000);
cur = tSafeDequeue(numberList);
    if(cur->isValid == 1)
    {
        gmp_printf("%Z i\n", cur->value);
        dummy++;
    }  
}
    return 1;
}
